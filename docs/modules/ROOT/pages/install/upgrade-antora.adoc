= Upgrade Antora

On this page, you'll learn:

* [x] How to upgrade the Antora CLI.
* [x] How to upgrade the default Antora site generator.

IMPORTANT: The CLI version and default site generator version must be the same.
If you follow these instructions when you upgrade, the versions will stay in sync.

== Upgrade the CLI

To upgrade the CLI, open a terminal and type:

 $ npm install -g @antora/cli

The Node package manager (npm) will automatically install the latest version of the CLI.

[TIP]
====
If you installed the CLI and the default site generator globally, you can upgrade both of them with the same command.

 $ npm install -g @antora/cli @antora/site-generator-default

====

.npm tips
****
To list the Node packages that are installed globally, type the following command in your terminal:

 $ npm ls -g --depth=0

If the CLI and site generator are installed globally, you'll see them listed in the terminal output.
The output will also show the CLI and site generator versions, which should be the same.

.List of globally installed Node packages
[subs=attributes+]
....
/home/user/.nvm/versions/node/v8.11.4/lib
├── @antora/cli@{page-component-version}.x
├── @antora/site-generator-default@{page-component-version}.x
├── npm@5.6.0
└── ...
....

To list the Node packages you have installed locally in a project, go to that directory and type:

 $ npm ls --depth=0

****

== Upgrade the default site generator

The method for upgrading the default site generator depends on whether it is installed globally or inside a playbook project.

If it's installed globally, the command to upgrade it is:

 $ npm install -g @antora/site-generator-default

If you installed the site generator in a playbook project, update the version number of the site generator in the package file.

. Switch to the playbook project directory.

. Open the [.path]_package.json_ file.

. Change the version number of the site generator.
+
[source,json,subs=attributes+]
----
{
  "dependencies": {
    "@antora/site-generator-default": "^{page-component-version}.0"
  }
}
----

. Save the file.

. Upgrade the site generator by running the `npm install` command.

 $ npm install

TIP: If you're using yarn instead of npm, run the `yarn` command after updating  [.path]_package.json_.
It may be necessary to pass the `--force` flag to force an update.

You've now upgraded to the latest version of the Antora CLI and default site generator.

== What's next?

* Checkout the https://gitlab.com/antora/antora/blob/master/CHANGELOG.adoc[CHANGELOG^] to see what's new.
* xref:run-antora.adoc[Run Antora] and generate a documentation site.
